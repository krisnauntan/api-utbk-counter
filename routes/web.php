<?php

use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'index'])->name('index');
Route::post('insert', [IndexController::class, 'insert'])->name('insert');

Route::get('max', [IndexController::class, 'max'])->name('max.index');
Route::post('max/insert', [IndexController::class, 'max_insert'])->name('max.insert');