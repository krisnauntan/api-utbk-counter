<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\TRController;
use App\Http\Controllers\API\JumlahController;
use App\Http\Controllers\API\CounterController;
use App\Http\Controllers\API\MaxController;
use App\Http\Controllers\API\RuanganController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('counter', [CounterController::class, 'index'])->name('counter.index');
Route::post('counter', [CounterController::class, 'store'])->name('counter.store');

Route::get('ruangan', [RuanganController::class, 'index'])->name('ruangan.index');
Route::post('ruangan', [RuanganController::class, 'store'])->name('ruangan.store');
Route::get('ruangan/{id}', [RuanganController::class, 'show'])->name('ruangan.show');
Route::put('ruangan/{id}', [RuanganController::class, 'update'])->name('ruangan.update');
Route::delete('ruangan/{id}', [RuanganController::class, 'delete'])->name('ruangan.delete');
Route::post('ruangan/import', [RuanganController::class, 'import'])->name('ruangan.import');

Route::get('tr', [TRController::class, 'index'])->name('tr.index');
Route::post('tr', [TRController::class, 'store'])->name('tr.store');
Route::get('tr/{id}', [TRController::class, 'show'])->name('tr.show');
Route::put('tr/{id}', [TRController::class, 'update'])->name('tr.update');
Route::delete('tr/{id}', [TRController::class, 'delete'])->name('tr.delete');
Route::post('tr/import', [TRController::class, 'import'])->name('tr.import');

Route::get('jumlah', [JumlahController::class, 'index'])->name('jumlah.index');

Route::get('max', [MaxController::class, 'index'])->name('max.index');
Route::post('max', [MaxController::class, 'store'])->name('max.store');
Route::get('max/{id}', [MaxController::class, 'show'])->name('max.show');
Route::put('max/{id}', [MaxController::class, 'update'])->name('max.update');
Route::delete('max/{id}', [MaxController::class, 'delete'])->name('max.delete');
Route::post('max/import', [MaxController::class, 'import'])->name('max.import');