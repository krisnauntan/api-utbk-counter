<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TR extends Model
{
    use HasFactory;
    protected $table = 'tr';
    public $guarded = [];

    public function ruangan_tr()
    {
        return $this->belongsToMany(Ruangan::class, 'ruangan_tr', 'tr_id', 'ruangan_id');
    }
}
