<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Max extends Model
{
    use HasFactory;
    protected $table = 'max';
    public $guarded = [];

    public function ruangan()
    {
        return $this->belongsTo(Ruangan::class);
    }
}
