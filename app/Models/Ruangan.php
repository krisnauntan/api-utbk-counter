<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
    use HasFactory;
    protected $table = 'ruangan';
    public $guarded = [];

    public function ruangan_tr()
    {
        return $this->belongsToMany(TR::class, 'ruangan_tr', 'ruangan_id', 'tr_id');
    }

    public function counter()
    {
        return $this->hasMany(Counter::class);
    }

    public function max()
    {
        return $this->hasMany(Max::class);
    }
}
