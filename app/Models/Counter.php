<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    use HasFactory;
    protected $table = 'counter';
    public $guarded = [];

    public function ruangan()
    {
        return $this->belongsTo(Ruangan::class);
    }

    public function tr()
    {
        return $this->belongsTo(TR::class);
    }
}
