<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RuanganTR extends Model
{
    use HasFactory;
    protected $table = 'ruangan_tr';
    public $guarded = [];
}
