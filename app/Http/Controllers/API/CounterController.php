<?php

namespace App\Http\Controllers\API;

use App\Models\Counter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CounterController extends Controller
{
    public function index(Request $request)
    {
        $data = Counter::with('ruangan', 'tr')
        ->when($request->filter, function($query) use ($request) {
            if($request->filter == 'today') {
                return $query->where('tanggal', date('Y-m-d'));
            }
        })
        ->when($request->tanggal, function($query) use ($request) {
            return $query->where('tanggal', $request->tanggal);
        })
        ->orderBy('ruangan_id')
        ->get();

        $data->each(function($item) {
            $item->max_1 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_1 ?? 20;
            $item->max_2 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_2 ?? 20;
        });

        return response()->json([
            'status' => 200,
            'message' => 'Data counter',
            'data' => $data,
        ]);
    }

    public function store(Request $request)
    {
        if($request->sesi == 1)
        {
            $counter = Counter::updateOrCreate([
                'ruangan_id' => $request->ruangan_id,
                'tr_id' => $request->tr_id,
                'tanggal' => date('Y-m-d'),
            ], [
                'sesi_1' => $request->jumlah,
            ]);
        }
        else
        {
            $counter = Counter::updateOrCreate([
                'ruangan_id' => $request->ruangan_id,
                'tr_id' => $request->tr_id,
                'tanggal' => date('Y-m-d'),
            ], [
                'sesi_2' => $request->jumlah,
            ]);
        }

        return response()->json([
            'status' => 201,
            'message' => 'Data berhasil disimpan',
            'data' => $counter,
        ]);
    }
}
