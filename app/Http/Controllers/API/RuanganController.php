<?php

namespace App\Http\Controllers\API;

use App\Models\Ruangan;
use App\Models\RuanganTR;
use Illuminate\Http\Request;
use App\Imports\RuanganImport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class RuanganController extends Controller
{
    public function index()
    {
        $ruangan = Ruangan::all();

        $ruangan->each(function($item) {
            $item->nama = $item->gedung . ' - ' . $item->ruang;
            $item->tr = $item->ruangan_tr->first();
        });

        return response()->json([
            'status' => 200,
            'message' => 'Data ruangan',
            'data' => $ruangan
        ], 200);
    }

    public function store(Request $request)
    {
        $ruangan = Ruangan::updateOrCreate([
            'srv' => $request->srv,
            'gedung' => $request->gedung,
            'ruang' => $request->ruang,
        ], [
            'srv' => $request->srv,
            'gedung' => $request->gedung,
            'ruang' => $request->ruang,
        ]);

        $ruangan_tr = RuanganTR::updateOrCreate([
            'ruangan_id' => $ruangan->id,
            'tr_id' => $request->tr_id,
        ], [
            'ruangan_id' => $ruangan->id,
            'tr_id' => $request->tr_id,
        ]);

        return response()->json([
            'status' => 201,
            'message' => 'Data ruangan berhasil disimpan',
            'data' => $ruangan,
            'data_tr' => $ruangan_tr,
        ], 201);
    }

    public function show($id)
    {
        $ruangan = Ruangan::find($id);

        return response()->json([
            'status' => 200,
            'message' => 'Detail ruangan',
            'data' => $ruangan
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $ruangan = Ruangan::find($id);

        $ruangan->update([
            'srv' => $request->srv,
            'gedung' => $request->gedung,
            'ruang' => $request->ruang,
        ]);

        $ruangan_tr = RuanganTR::where('ruangan_id', $id)->first();

        $ruangan_tr->update([
            'tr_id' => $request->tr_id,
        ]);

        return response()->json([
            'status' => 201,
            'message' => 'Data ruangan berhasil diupdate',
            'data' => $ruangan,
            'data_tr' => $ruangan_tr,
        ], 201);
    }

    public function delete($id)
    {
        $ruangan = Ruangan::find($id);

        $ruangan->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Data ruangan berhasil dihapus',
        ], 200);
    }

    public function import(Request $request)
    {
        $file = $request->file('file');

        Excel::import(new RuanganImport, $file);

        return response()->json([
            'status' => 200,
            'message' => 'Data ruangan berhasil diimport',
        ], 200);
    }
}
