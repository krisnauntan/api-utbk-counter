<?php

namespace App\Http\Controllers\API;

use App\Models\Max;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\MaxImport;
use Maatwebsite\Excel\Facades\Excel;

class MaxController extends Controller
{
    public function index()
    {
        $max = Max::all();

        return response()->json([
            'status' => 200,
            'message' => 'Data max',
            'data' => $max
        ], 200);
    }

    public function store(Request $request)
    {
        $max = Max::updateOrCreate([
            'ruangan_id' => $request->ruangan_id,
            'tanggal' => $request->tanggal,
        ], [
            'ruangan_id' => $request->ruangan_id,
            'tanggal' => $request->tanggal,
        ]);

        return response()->json([
            'status' => 201,
            'message' => 'Data max berhasil disimpan',
            'data' => $max
        ], 201);
    }

    public function show($id)
    {
        $max = Max::find($id);

        return response()->json([
            'status' => 200,
            'message' => 'Detail max',
            'data' => $max
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $max = Max::find($id);

        $max->update([
            'ruangan_id' => $request->ruangan_id,
            'tanggal' => $request->tanggal,
        ]);

        return response()->json([
            'status' => 201,
            'message' => 'Data max berhasil diupdate',
            'data' => $max
        ], 201);
    }

    public function delete($id)
    {
        $max = Max::find($id);

        $max->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Data max berhasil dihapus',
        ], 200);
    }

    public function import(Request $request)
    {
        $file = $request->file('file');

        Excel::import(new MaxImport, $file);

        return response()->json([
            'status' => 200,
            'message' => 'Data max berhasil diimport',
        ], 200);
    }
}
