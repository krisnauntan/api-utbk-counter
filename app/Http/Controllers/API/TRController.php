<?php

namespace App\Http\Controllers\API;

use App\Models\TR;
use App\Imports\TRImport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ruangan;
use App\Models\RuanganTR;
use Maatwebsite\Excel\Facades\Excel;

class TRController extends Controller
{
    public function index()
    {
        $tr = TR::all();

        return response()->json([
            'status' => 200,
            'message' => 'Data tr',
            'data' => $tr
        ], 200);
    }

    public function store(Request $request)
    {
        $tr = TR::updateOrCreate([
            'nama' => $request->nama,
        ], [
            'nama' => $request->nama,
        ]);

        return response()->json([
            'status' => 201,
            'message' => 'Data tr berhasil disimpan',
            'data' => $tr
        ], 201);
    }

    public function show($id)
    {
        $tr = TR::find($id);

        return response()->json([
            'status' => 200,
            'message' => 'Detail tr',
            'data' => $tr
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $tr = TR::find($id);

        $tr->update([
            'nama' => $request->nama,
        ]);

        return response()->json([
            'status' => 201,
            'message' => 'Data tr berhasil diupdate',
            'data' => $tr
        ], 201);
    }

    public function delete($id)
    {
        $tr = TR::find($id);

        $tr->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Data tr berhasil dihapus',
        ], 200);
    }

    public function import(Request $request)
    {
        $file = $request->file('file');

        Excel::import(new TRImport, $file);

        return response()->json([
            'status' => 200,
            'message' => 'Data tr berhasil diimport',
        ], 200);
    }
}
