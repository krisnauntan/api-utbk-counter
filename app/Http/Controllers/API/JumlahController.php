<?php

namespace App\Http\Controllers\API;

use App\Models\Counter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JumlahController extends Controller
{
    public function index(Request $request)
    {
        $output = [];

        if($request->tanggal) {
            $tanggal = $request->tanggal;
            $data_server_1 = Counter::with('ruangan', 'tr')->whereHas('ruangan', function($query) {
                return $query->where('srv', 1);
            })
            ->where('tanggal', $tanggal)
            ->get();

            $data_server_1->each(function($item) {
                $item->max_1 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_1 ?? 20;
                $item->max_2 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_2 ?? 20;
            });
        
            $data_server_2 = Counter::with('ruangan', 'tr')->whereHas('ruangan', function($query) {
                return $query->where('srv', 2);
            })
            ->where('tanggal', $tanggal)
            ->get();

            $data_server_2->each(function($item) {
                $item->max_1 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_1 ?? 20;
                $item->max_2 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_2 ?? 20;
            });

            $data = Counter::with('ruangan', 'tr')
            ->where('tanggal', $tanggal)
            ->get();

            $data->each(function($item) {
                $item->max_1 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_1 ?? 20;
                $item->max_2 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_2 ?? 20;
            });

            $output[] = [
                'tanggal' => $tanggal,
                'server_1' => [
                    'sesi_1' => $data_server_1->sum('sesi_1'),
                    'sesi_2' => $data_server_1->sum('sesi_2'),
                    'total' => $data_server_1->sum('sesi_1') + $data_server_1->sum('sesi_2'),
                    'max_1' => $data_server_1->sum('max_1'),
                    'max_2' => $data_server_1->sum('max_2'),
                ],
                'server_2' => [
                    'sesi_1' => $data_server_2->sum('sesi_1'),
                    'sesi_2' => $data_server_2->sum('sesi_2'),
                    'total' => $data_server_2->sum('sesi_1') + $data_server_2->sum('sesi_2'),
                    'max_1' => $data_server_2->sum('max_1'),
                    'max_2' => $data_server_2->sum('max_2'),
                ],
                'total' => [
                    'sesi_1' => $data->sum('sesi_1'),
                    'sesi_2' => $data->sum('sesi_2'),
                    'total' => $data->sum('sesi_1') + $data->sum('sesi_2'),
                    'max_1' => $data->sum('max_1'),
                    'max_2' => $data->sum('max_2'),
                ],
            ];

            return response()->json([
                'status' => 200,
                'message' => 'Data jumlah',
                'data' => $output,
            ]);
        }
        
        $tanggals = Counter::pluck('tanggal')->unique();
        
        foreach($tanggals as $tanggal) {
            $data_server_1 = Counter::with('ruangan', 'tr')->whereHas('ruangan', function($query) {
                return $query->where('srv', 1);
            })
            ->where('tanggal', $tanggal)
            ->get();

            $data_server_1->each(function($item) {
                $item->max_1 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_1 ?? 20;
                $item->max_2 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_2 ?? 20;
            });
        
            $data_server_2 = Counter::with('ruangan', 'tr')->whereHas('ruangan', function($query) {
                return $query->where('srv', 2);
            })
            ->where('tanggal', $tanggal)
            ->get();

            $data_server_2->each(function($item) {
                $item->max_1 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_1 ?? 20;
                $item->max_2 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_2 ?? 20;
            });

            $data = Counter::with('ruangan', 'tr')
            ->where('tanggal', $tanggal)
            ->get();

            $data->each(function($item) {
                $item->max_1 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_1 ?? 20;
                $item->max_2 = $item->ruangan->max->where('tanggal', $item->tanggal)->first()->max_2 ?? 20;
            });

            $output[] = [
                'tanggal' => $tanggal,
                'server_1' => [
                    'sesi_1' => $data_server_1->sum('sesi_1'),
                    'sesi_2' => $data_server_1->sum('sesi_2'),
                    'total' => $data_server_1->sum('sesi_1') + $data_server_1->sum('sesi_2'),
                    'max_1' => $data_server_1->sum('max_1'),
                    'max_2' => $data_server_1->sum('max_2'),
                ],
                'server_2' => [
                    'sesi_1' => $data_server_2->sum('sesi_1'),
                    'sesi_2' => $data_server_2->sum('sesi_2'),
                    'total' => $data_server_2->sum('sesi_1') + $data_server_2->sum('sesi_2'),
                    'max_1' => $data_server_2->sum('max_1'),
                    'max_2' => $data_server_2->sum('max_2'),
                ],
                'total' => [
                    'sesi_1' => $data->sum('sesi_1'),
                    'sesi_2' => $data->sum('sesi_2'),
                    'total' => $data->sum('sesi_1') + $data->sum('sesi_2'),
                    'max_1' => $data->sum('max_1'),
                    'max_2' => $data->sum('max_2'),
                ],
            ];
        }
    
        return response()->json([
            'status' => 200,
            'message' => 'Data jumlah',
            'data' => $output,
        ]);
    }
}
