<?php

namespace App\Http\Controllers;

use App\Models\Counter;
use App\Models\Max;
use App\Models\Ruangan;
use App\Models\TR;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $ruangan = Ruangan::all();
        $tr = TR::all();
        $counter = Counter::where('tanggal', date('Y-m-d'))->orderBy('ruangan_id')->get();

        return view('index', compact('ruangan', 'tr', 'counter'));
    }
    
    public function insert(Request $request)
    {
        if($request->sesi == 1)
        {
            Counter::updateOrCreate([
                'ruangan_id' => $request->ruangan_id,
                'tr_id' => $request->tr_id,
                'tanggal' => date('Y-m-d'),
            ], [
                'sesi_1' => $request->jumlah,
            ]);
        }
        else
        {
            Counter::updateOrCreate([
                'ruangan_id' => $request->ruangan_id,
                'tr_id' => $request->tr_id,
                'tanggal' => date('Y-m-d'),
            ], [
                'sesi_2' => $request->jumlah,
            ]);
        }

        return redirect()->back()->with('success', 'Data berhasil disimpan');
    }

    public function max()
    {
        $ruangan = Ruangan::all();
        $max = Max::where('tanggal', date('Y-m-d'))->get();

        return view('max', compact('ruangan', 'max'));
    }

    public function max_insert(Request $request)
    {
        Max::updateOrCreate([
            'ruangan_id' => $request->ruangan_id,
            'tanggal' => $request->tanggal,
        ], [
            'max_1' => $request->max_1,
            'max_2' => $request->max_2,
        ]);

        return redirect()->back()->with('success', 'Data berhasil disimpan');
    }
}
