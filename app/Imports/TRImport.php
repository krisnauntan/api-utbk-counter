<?php

namespace App\Imports;

use App\Models\TR;
use Maatwebsite\Excel\Concerns\ToModel;

class TRImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new TR([
            'nama' => $row[0],
        ]);
    }
}
