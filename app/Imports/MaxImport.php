<?php

namespace App\Imports;

use App\Models\Max;
use Maatwebsite\Excel\Concerns\ToModel;

class MaxImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Max([
            'ruangan_id' => $row[0],
            'tanggal' => $row[1],
            'max_1' => $row[2],
            'max_2' => $row[3],
        ]);
    }
}
