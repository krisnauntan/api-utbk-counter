<?php

namespace App\Imports;

use App\Models\Ruangan;
use App\Models\RuanganTR;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class RuanganImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            $ruangan = Ruangan::create([
                'srv' => $row[0],
                'gedung' => $row[1],
                'ruang' => $row[2],
            ]);

            RuanganTR::create([
                'ruangan_id' => $ruangan->id,
                'tr_id' => $row[3],
            ]);
        }
    }
}
