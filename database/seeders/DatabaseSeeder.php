<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Ruangan;
use App\Models\RuanganTR;
use App\Models\TR;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        // Ruangan
        $data_ruangan = [
            [
                'id' => '1',
                'nama_ruangan' => 'Ruang A1',
            ], [
                'id' => '2',
                'nama_ruangan' => 'Ruang A2',
            ], [
                'id' => '3',
                'nama_ruangan' => 'Ruang Lab UTBK',
            ], [
                'id' => '4',
                'nama_ruangan' => 'Ruang Bahasa 1',
            ], [
                'id' => '5',
                'nama_ruangan' => 'Ruang Bahasa 2',
            ], [
                'id' => '6',
                'nama_ruangan' => 'Labkom Ruang A',
            ], [
                'id' => '7',
                'nama_ruangan' => 'Labkom Ruang B',
            ], [
                'id' => '8',
                'nama_ruangan' => 'Labkom Ruang C',
            ], [
                'id' => '9',
                'nama_ruangan' => 'R. Lab Sisfo',
            ], [
                'id' => '10',
                'nama_ruangan' => 'R. Komputer Terpadu A1',
            ], [
                'id' => '11',
                'nama_ruangan' => 'R. Komputer Terpadu A2',
            ], [
                'id' => '12',
                'nama_ruangan' => 'R. Komputer Terpadu B1',
            ], [
                'id' => '13',
                'nama_ruangan' => 'R. Komputer Terpadu B2',
            ], [
                'id' => '14',
                'nama_ruangan' => 'E-Learning 1',
            ], [
                'id' => '15',
                'nama_ruangan' => 'E-Learning 2',
            ], [
                'id' => '16',
                'nama_ruangan' => 'E-Learning 3',
            ], [
                'id' => '17',
                'nama_ruangan' => 'E-Learning 4',
            ], [
                'id' => '18',
                'nama_ruangan' => 'Fisip 1',
            ], [
                'id' => '19',
                'nama_ruangan' => '36823',
            ], [
                'id' => '20',
                'nama_ruangan' => '37288',
            ], [
                'id' => '21',
                'nama_ruangan' => 'FP-1',
            ], [
                'id' => '22',
                'nama_ruangan' => 'Informatika 1',
            ], [
                'id' => '23',
                'nama_ruangan' => 'Informatika 2',
            ], [
                'id' => '24',
                'nama_ruangan' => 'Lab Uji Kompetensi Perpustakaan Lama Lantai 2 - Ruang 1',
            ], [
                'id' => '25',
                'nama_ruangan' => 'Lab Uji Kompetensi Perpustakaan Lama Lantai 2 - Ruang 2',
            ], [
                'id' => '26',
                'nama_ruangan' => 'Lab Uji Kompetensi Perpustakaan Lama Lantai 2 - Ruang 3',
            ], [
                'id' => '27',
                'nama_ruangan' => 'Lab Uji Kompetensi Perpustakaan Lama Lantai 2 - Ruang 4',
            ], [
                'id' => '28',
                'nama_ruangan' => 'Lab Uji Kompetensi Perpustakaan Lama Lantai 2 - Ruang 5',
            ], [
                'id' => '29',
                'nama_ruangan' => 'Lab Uji Kompetensi Perpustakaan Lama Lantai 2 - Ruang 6',
            ], [
                'id' => '30',
                'nama_ruangan' => 'Lab Uji Kompetensi Perpustakaan Lama Lantai 2 - Ruang 7',
            ], [
                'id' => '31',
                'nama_ruangan' => 'Lab Uji Kompetensi Perpustakaan Lama Lantai 2 - Ruang 8',
            ], [
                'id' => '32',
                'nama_ruangan' => 'Perpustakaan Lama LT. 1 - Ruang Lab UTBK 1',
            ], [
                'id' => '33',
                'nama_ruangan' => 'Perpustakaan Lama LT. 1 - Ruang Lab UTBK 2',
            ], [
                'id' => '34',
                'nama_ruangan' => 'Perpustakaan Lama LT. 1 - Ruang Lab UTBK 3',
            ], [
                'id' => '35',
                'nama_ruangan' => 'Perpustakaan Lama LT. 1 - Ruang Lab UTBK 4',
            ]
        ];
        Ruangan::insert($data_ruangan);

        // TR
        $data_tr = [
            [
                'id' => '1',
                'nama_tr' => 'Aris Fajrianto',
            ], [
                'id' => '2',
                'nama_tr' => 'Achmad Yunizar',
            ]
        ];
        TR::insert($data_tr);

        // Ruangan TR
        $data_ruangan_tr = [
            [
                'ruangan_id' => '1',
                'tr_id' => '1',
            ],
        ];
        RuanganTR::insert($data_ruangan_tr);
    }
}
