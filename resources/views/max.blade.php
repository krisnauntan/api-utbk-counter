<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>API UTBK Counter</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-4">
        <div class="container">
            <a class="navbar-brand" href="#">API UTBK Counter</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                <a class="nav-link" href="{{ route('index') }}">Index</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="{{ route('max.index') }}">Max</a>
                </li>
            </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <form action="{{ route('max.insert') }}" method="post">
            @csrf
            <div>
                <select name="ruangan_id" required>
                    <option value="" selected disabled>Ruangan</option>
                    @foreach ($ruangan as $item)
                        <option value="{{ $item->id }}">{{ $item->gedung }} - {{ $item->ruang }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <input type="date" name="tanggal">
            </div>
            <div>
                <input type="number" name="max_1" placeholder="Maksimal Peserta Sesi 1">
            </div>
            <div>
                <input type="number" name="max_2" placeholder="Maksimal Peserta Sesi 2">
            </div>
            <input type="submit" value="Simpan">
        </form>
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <table class="table table-bordered">
            <tr>
                <td>No</td>
                <td>Ruangan</td>
                <td>Tanggal</td>
                <td>Maksimal Sesi 1</td>
                <td>Maksimal Sesi 2</td>
            </tr>
            @foreach ($max as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->ruangan->gedung }} - {{ $item->ruangan->ruang }}</td>
                    <td>{{ $item->tanggal }}</td>
                    <td>{{ $item->max_1 }}</td>
                    <td>{{ $item->max_2 }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
  </body>
</html>